<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();

use App\Hobby\Message;

$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobby Add Form</title>
</head>
<body>

<form action="store.php" method="post">
    Please Enter Your Name:
    <input type="text" name="name">
    <br>

    Please Enter Your Hobbies:
    <br>
    <input type="checkbox" name="hobby" value="Programming" checked>Programming<br>
    <input type="checkbox" name="hobby" value="Reading Books">Reading Books<br>
    <input type="checkbox" name="hobby" value="Writing">Writing<br>
    <input type="checkbox" name="hobby" value="Singing">Singing<br>
    <input type="checkbox" name="hobby" value="Dancing">Dancing<br>
    <input type="checkbox" name="hobby" value="Painting">Painting<br>
    <input type="checkbox" name="hobby" value="Gardening">Gardening<br>
    <input type="checkbox" name="hobby" value="Travelling">Travelling<br>
    <input type="checkbox" name="hobby" value="Watching Movies">Watching Movies<br>
    <input type="checkbox" name="hobby" value="Listening Songs">Listening Songs<br>
    <input type="checkbox" name="hobby" value="Facebooking">Facebooking<br>
    <br>

    <input type="submit">

</form>

<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>