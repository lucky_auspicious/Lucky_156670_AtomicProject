<?php

namespace App\Hobby;
use PDO;
use PDOException;


class Database
{

   public $DBH;


   public function __construct()
   {

      try {

         $this->DBH = new PDO('mysql:host=localhost;dbname=lucky_156670_atomic_project_b44', "root", "");
         $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );


      } catch (PDOException $error) {

         print "Database Error!: " . $error->getMessage() . "<br/>";
         die();
      }


   }



}