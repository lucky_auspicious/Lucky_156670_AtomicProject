<?php

namespace App\Birthday;

use App\Birthday\Message;
use App\Birthday\Utility;

use App\Birthday\Database as DB;
use PDO;

class Birthday extends DB
{
       private $id;
       private $birth_date;

    public function setData($postData){

         if(array_key_exists('id',$postData)){
             $this->id = $postData['id'];
         }

         if(array_key_exists('birthDate',$postData)){
             $this->birth_date = $postData['birthDate'];
         }
     }

      public function store(){

          $arrData = array($this->birth_date);

          $sql = "INSERT into birthday(birth_date) VALUES(?)";

          $STH = $this->DBH->prepare($sql);

          $result =$STH->execute($arrData);

          if($result)
              Message::message("Success! Data Has Been Inserted Successfully :)");
          else
              Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

          Utility::redirect('create.php');


      }


    public function index(){

        $sql = "select * from birthday where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from birthday where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from birthday where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}