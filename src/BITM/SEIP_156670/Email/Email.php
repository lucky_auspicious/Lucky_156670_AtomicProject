<?php

namespace App\Email;

use App\Email\Message;
use App\Email\Utility;

use App\Email\Database as DB;
use PDO;

class Email extends DB
{
       private $id;
       private $name;
       private $email_id;

    public function setData($postData){

         if(array_key_exists('id',$postData)){
             $this->id = $postData['id'];
         }

         if(array_key_exists('name',$postData)){
             $this->name = $postData['name'];
         }

         if(array_key_exists('email',$postData)){
             $this->email_id = $postData['email'];
         }
     }

    /**
     *
     */
    public function store(){

          $arrData = array($this->name,$this->email_id);

          $sql = "INSERT into email(name,email_id) VALUES(?,?)";

          $STH = $this->DBH->prepare($sql);

          $result =$STH->execute($arrData);

          if($result)
              Message::message("Success! Data Has Been Inserted Successfully :)");
          else
              Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

          Utility::redirect('create.php');


      }



    public function index(){

        $sql = "select * from email where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from email where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from email where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}