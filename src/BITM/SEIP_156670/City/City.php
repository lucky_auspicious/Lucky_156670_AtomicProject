<?php

namespace App\City;

use App\City\Message;
use App\City\Utility;

use App\City\Database as DB;
use PDO;

class City extends DB
{
       private $id;
       private $city_name;

    public function setData($postData){

         if(array_key_exists('id',$postData)){
             $this->id = $postData['id'];
         }

         if(array_key_exists('cityName',$postData)){
             $this->city_name = $postData['cityName'];
         }
     }

      public function store(){

          $arrData = array($this->city_name);

          $sql = "INSERT into city(city_name) VALUES(?)";

          $STH = $this->DBH->prepare($sql);

          $result =$STH->execute($arrData);

          if($result)
              Message::message("Success! Data Has Been Inserted Successfully :)");
          else
              Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

          Utility::redirect('create.php');


      }



    public function index(){

        $sql = "select * from city where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from city where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from city where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}