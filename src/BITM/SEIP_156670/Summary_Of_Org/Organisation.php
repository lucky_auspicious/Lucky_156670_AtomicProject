<?php

namespace App\Summary_Of_Org;

use App\Summary_Of_Org\Message;
use App\Summary_Of_Org\Utility;

use App\Summary_Of_Org\Database as DB;
use PDO;

class Organisation extends DB
{
       private $id;
       private $organisation_name;
       private $summary;

    public function setData($postData){

         if(array_key_exists('id',$postData)){
             $this->id = $postData['id'];
         }

         if(array_key_exists('organisationName',$postData)){
             $this->organisation_name = $postData['organisationName'];
         }

        if(array_key_exists('summary',$postData)){
            $this->summary = $postData['summary'];
        }
     }

      public function store(){

          $arrData = array($this->organisation_name,$this->summary);

          $sql = "INSERT into organisation_summary(organisation_name,summary) VALUES(?,?)";

          $STH = $this->DBH->prepare($sql);

          $result =$STH->execute($arrData);

          if($result)
              Message::message("Success! Data Has Been Inserted Successfully :)");
          else
              Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

          Utility::redirect('create.php');


      }



    public function index(){

        $sql = "select * from organisation_summary where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from organisation_summary where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from organisation_summary where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}