<?php

namespace App\BookTitle;

use App\BookTitle\Message;
use App\BookTitle\Utility;

use App\BookTitle\Database as DB;
use PDO;

class BookTitle extends DB
{
       private $id;
       private $book_name;
       private $author_name;

    public function setData($postData){

         if(array_key_exists('id',$postData)){
             $this->id = $postData['id'];
         }

         if(array_key_exists('bookName',$postData)){
             $this->book_name = $postData['bookName'];
         }

         if(array_key_exists('authorName',$postData)){
             $this->author_name = $postData['authorName'];
         }
     }

      public function store(){

          $arrData = array($this->book_name,$this->author_name);

          $sql = "INSERT into book_title(book_name,author_name) VALUES(?,?)";

          $STH = $this->DBH->prepare($sql);

          $result =$STH->execute($arrData);

          if($result)
              Message::message("Success! Data Has Been Inserted Successfully :)");
          else
              Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

          Utility::redirect('create.php');


      }



    public function index(){

        $sql = "select * from book_title where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from book_title where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from book_title where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }

}